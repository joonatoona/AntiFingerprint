"use strict";

const USER_AGENTS = [
    {
        "ua": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
        "av": "5.0 (Windows NT 10.0)",
        "os": "Windows NT 10.0; Win64; x64",
        "platform": "Win32"
    },
    {
        "ua": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
        "av": "5.0 (Windows NT 6.1)",
        "platform": "Win32"
    },
    {
        "ua": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36",
        "av": "5.0 (Windows NT 10.0)",
        "os": "Windows NT 10.0; Win64; x64",
        "platform": "Win32"
    },
    {
        "ua": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0",
        "av": "5.0 (Windows NT 10.0)",
        "os": "Windows NT 10.0; Win64; x64",
        "platform": "Win32"
    },
    {
        "ua": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
        "av": "5.0 (Macintosh)",
        "os": "Intel Mac OS X 10_13_2",
        "platform": "Intel Mac OS X 10_13_2"
    },
    {
        "ua": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7",
        "av": "5.0 (Macintosh)",
        "os": "Intel Mac OS X 10_13_2",
        "platform": "Intel Mac OS X 10_13_2"
    },
    {
        "ua": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
        "av": "5.0 (Macintosh)",
        "os": "Intel Mac OS X 10_12_6",
        "platform": "Intel Mac OS X 10_12_6"
    },
    {
        "ua": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36",
        "av": "5.0 (Windows NT 6.1)",
        "os": "Windows NT 6.1; Win64; x64",
        "platform": "Win32"
    },
    {
        "ua": "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0",
        "av": "5.0 (Windows NT 6.1)",
        "os": "Windows NT 6.1; Win64; x64",
        "platform": "Win32"
    },
    {
        "ua": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0",
        "av": "5.0 (X11)",
        "os": "Linux x86_64",
        "platform": "Linux x86_64"
    }
]

function randomElement(arr) {
    return arr[Math.floor(Math.random() * arr.length)]
}

function modifyHeaders(e) {
    for (let header of e.requestHeaders) {
        if (header.name.toLowerCase() === "user-agent") {
            let newUA = randomElement(USER_AGENTS);
            header.value = newUA.ua;
        }
    }
    return {requestHeaders: e.requestHeaders}
}

browser.webRequest.onBeforeSendHeaders.addListener(
    modifyHeaders,
    { urls: ["<all_urls>"] },
    ["blocking", "requestHeaders"]
);
